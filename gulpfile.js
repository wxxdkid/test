var gulp = require('gulp');
var plug = require('gulp-load-plugins')({lazy: true});
var config = require('./gulp.config')();
var glob = require('glob');
var plato = require('plato');
var merge = require('merge-stream');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

/**
 * @desc Create, connect to the server
 */
gulp.task('connect', ['watch'], function () {
    log('Starting BrowserSync on port ' + config.browserSync.port);
    browserSync.init(config.browserSync);
});

/**
 * @desc Watch files
 */
gulp.task('watch', function () {
    gulp.watch([config.js, config.html]).on('change', browserSync.reload);
    gulp.watch(config.scss, ['sass']);
});

/**
 * @desc Create $templateCache from the html templates
 */
gulp.task('templatecache', function () {
    log('Creating an AngularJS $templateCache.');
    return gulp
        .src(config.html)
        .pipe(plug.minifyHtml({empty: true}))
        .pipe(plug.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
        ))
        .pipe(gulp.dest(config.build + 'js'));
});

/**
 * @desc Watch scss files and compile
 */
gulp.task('sass', function () {
    log('Compiling Sass --> CSS.');
    return gulp.src(config.clientApp + 'main.scss')
        .pipe(plug.sass().on('error', plug.sass.logError))
        .pipe(gulp.dest(config.client + 'css'))
        .pipe(reload({stream: true}));
});

/**
 * @desc Minify and bundle the CSS
 */
gulp.task('css', ['sass'], function () {
    log('Compiling Sass --> CSS and optimizing.');
    return gulp.src(config.css)
        .pipe(plug.concat('all.min.css'))
        .pipe(plug.autoprefixer('last 2 version', '> 5%'))
        .pipe(plug.cleanCss({compatibility: '*'}))
        .pipe(gulp.dest(config.build + 'css'));
});

/**
 * @desc Minify and bundle the Vendor CSS
 */
gulp.task('vendorcss', function () {
    log('Optimizing vendor CSS.');
    return gulp.src(config.vendorcss)
        .pipe(plug.concat('vendor.min.css'))
        .pipe(plug.cleanCss({compatibility: '*'}))
        .pipe(gulp.dest(config.build + 'css'));
});

/**
 * @desc Minify and bundle the app's JavaScript
 */
gulp.task('js', ['templatecache'], function () {
    var source = [].concat(config.js);
    log('Optimizing JS.');

    return gulp
        .src(source)
        .pipe(plug.concat('all.min.js'))
        .pipe(plug.ngAnnotate({add: true, single_quotes: true}))
        .pipe(plug.uglify({mangle: true}))
        .pipe(gulp.dest(config.build + 'js'));
});

/**
 * @desc Copy the Vendor JavaScript
 */
gulp.task('vendorjs', function () {
    log('Optimizing vendor JS.');
    return gulp.src(config.vendorjs)
        .pipe(plug.concat('vendor.min.js'))
        .pipe(plug.uglify())
        .pipe(gulp.dest(config.build + 'js'));
});

/**
 * Remove all files from the build folder
 * One way to run clean before all tasks is to run
 * from the cmd line: npm run clean.
 * @return {Stream}
 */
gulp.task('clean', function () {
    return gulp.src(['build'])
        .pipe(plug.clean());
});

/**
 * Inject all the files into the new index.html
 * rev, but no map
 */
gulp.task('rev-and-inject', ['js', 'vendorjs', 'css', 'vendorcss'], function () {
    var minified = config.build + '**/*.min.*';
    var index = config.client + 'index.html';
    var minFilter = plug.filter(['**/*.min.*', '!**/*.map']);
    var indexFilter = plug.filter(['index.html']);
    log('Making revisions of css and js files, injecting them into index.html.');

    return gulp
        .src([].concat(minified, index))  // add all built min files and index.html
        .pipe(gulp.dest(config.build)) // write the rev files
        .pipe(minFilter.restore()) // remove filter, back to original stream
        // inject the files into index.html
        .pipe(indexFilter) // filter to index.html
        .pipe(inject('css/vendor.min.css', 'inject-vendor'))
        .pipe(inject('css/all.min.css'))
        .pipe(inject('js/vendor.min.js', 'inject-vendor'))
        .pipe(inject('js/all.min.js'))
        .pipe(inject('js/templates.js', 'inject-templates'))
        .pipe(gulp.dest(config.build)) // write the rev files
        .pipe(indexFilter.restore()); // remove filter, back to original stream

    function inject(path, name) {
        var time = new Date().getTime();
        var glob = config.build + path;
        var options = {
            ignorePath: config.build.substring(1),
            addRootSlash: false,
            transform: function (filepath) {
                arguments[0] = filepath + '?v=' + time;
                return plug.inject.transform.apply(inject.transform, arguments);
            }
        };
        if (name) {
            options.name = name;
        }
        return plug.inject(gulp.src(glob), options);
    }
});

/**
 * Wire-up the bower dependencies
 * @return {Stream}
 */
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;
    gulp.src(config.client + 'index.html')
        .pipe(wiredep({
            directory: config.bower.directory
        }))
        .pipe(gulp.dest(config.client));
});

/**
 * Build the optimized app
 */
gulp.task('build', ['rev-and-inject'], function () {
    log('Building everything...');
    return gulp.src('').pipe(plug.notify({
        onLast: true,
        message: 'Deployed code!'
    }));
});

/**
 * Log a message or series of messages using chalk's blue color.
 * Can pass in a string, object or array.
 */
function log(msg) {
    var item;
    if (typeof (msg) === 'object') {
        for (item in msg) {
            if (msg.hasOwnProperty(item)) {
                plug.util.log(plug.util.colors.blue(msg[item]));
            }
        }
    } else {
        plug.util.log(plug.util.colors.blue(msg));
    }
}

/**
 * @desc Default gulp task
 */
gulp.task('default', ['connect', 'sass']);
