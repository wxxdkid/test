module.exports = function () {

    var client = './public/';
    var clientApp = 'public/app/';
    var build = './build/';
    var report = './report/';
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({dependencies: true})['js'];
    var bower = {
        json: require('./bower.json'),
        directory: './public/bower_components/',
        ignorePath: '/public/'
    };
    var config = {
        client: client,
        clientApp: clientApp,
        index: clientApp + 'index.html',
        bower: bower,
        html: [
            clientApp + '**/*.html'
        ],
        css: [
            client + 'css/*.css'
        ],
        vendorcss: [
        ],
        js: [
            clientApp + '**/*.module.js',
            clientApp + '**/*.js'
        ],
        jsOrder: [
            '**/app.module.js',
            '**/*.module.js',
            '**/*.js'
        ],
        vendorjs: bowerFiles,

        scss: [
            clientApp + '**/*.scss'
        ],
        build: build,

        /** template cache */
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'app.core',
                root: 'app/',
                standalone: false
            }
        },

        browserSync: {
            notify: false,
            port: 3000,
            server: {
                baseDir: [
                    'public'
                ]
            }
        }
    };

    return config;

};
