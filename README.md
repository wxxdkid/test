#Skore Enterprise

> *Preview dev: http://skoreappdev.com*

> *Preview pre: https://skoreapppreprod.com*

> *Preview prod: https://app.justskore.it*

## Structure
	/build 	(created on the fly)
    /client
        /app
            /components
            /content
            /core
            /graphmanager
            /lib
            /scss
            /services
            /templates

## Requirements
> The following are need to be installed:
  - node
  - npm
  - git
  - bower
  - gulp

### The Modules
The app has feature modules and depends on a series of external modules and custom but cross-app modules

```
app --> [
        app.core,
        app.theme,
        app.auth,
        app.admin,
        app.process,
        app.user,
        app.workspace,
		app.core --> [
        blocks.services,
        blocks.request,
        blocks.components,
        blocks.logger,
        blocks.exception,
        blocks.router,
		]
    ]
```

## core Module
Core modules are ones that are shared throughout the entire application and may be customized.

This is an aggregator of modules that the application will need. The `core` module takes the blocks, common, and Angular sub-modules as dependencies. 

## blocks Modules
Block modules are reusable blocks of code that can be used across projects simply by including them as dependencies.

### blocks.logger Module
The `blocks.logger` module handles logging across the Angular app.

### blocks.exception Module
The `blocks.exception` module handles exceptions across the Angular app.

It depends on the `blocks.logger` module, because the implementation logs the exceptions.

### blocks.router Module
The `blocks.router` module contains a routing helper module that assists in adding routes to the $stateProvider.

### Quick Start
> Node and git needs to be installed.
1. Install globally
    - npm install bower -g
    - npm install gulp -g
    - npm install (root directory)
    - bower install (root directory)
    
    
2. Run server, load it in the browser
    `npm start`
    
    This loads http://localhost:3000/
    
> See `gulp` configurations in gulp.config.js

### Analyze
> Run `npm run analyze` to analyze your code with JSHint and JSCS, visualize it with Plato.

### Build
> Run `npm run build` to clean up your directory, concatenate and optimize your application 
and to copy all necessary libraries, fonts, images(optimized), etc.

