(function () {
    'use strict';

    angular.module('app.core', [
        'blocks.request',
        'blocks.logger',
        'blocks.exception',
        'blocks.router'
    ]);

})();
