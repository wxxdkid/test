(function () {
    'use strict';

    angular
        .module('blocks.router')
        .provider('routehelperConfig', routehelperConfig)
        .factory('routehelper', routehelper);

    /* @ngInject */
    function routehelperConfig($stateProvider, $urlRouterProvider) {
        this.config = {
            $routeProvider: $stateProvider,
            $urlRouterProvider: $urlRouterProvider
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    }

    /* @ngInject */
    function routehelper(routehelperConfig) {
        var $routeProvider = routehelperConfig.config.$routeProvider;

        var service = {
            configureRoutes: configureRoutes
        };

        return service;

        /** @description Loop states of all our modules which are inside config.route */
        function configureRoutes(routes) {
            routes.forEach(function (route) {
                route.config.resolve =
                    angular.extend(route.config.resolve || {}, routehelperConfig.config.resolveAlways);
                $routeProvider.state(route.name, route.config);
            });
            routehelperConfig.config.$urlRouterProvider.otherwise('/dashboard');
        }


    }
})();
