(function () {
    'use strict';

    angular
        .module('request.url', [])
        .factory('url', [
            function () {

                var baseUrl = 'https://localhost:8080/';

                return {

                    site: {
                        getUser:       baseUrl + 'user/get-current-user',
                        login:         baseUrl + 'user/login',
                        getPrivacies:  baseUrl + 'admin-terms/view',
                        resetPassword: baseUrl + 'user/password-reset-request'
                    }

                };
            }
        ]);
})();
