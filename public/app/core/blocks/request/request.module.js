(function () {
    'use strict';

    angular.module('blocks.request', [
        'request.url',
        'request.handler'
    ]);

})();
