(function () {
    'use strict';

    angular
        .module('request.handler', ['ngStorage'])
        .service('http', http);

    /* @ngInject */
    function http($http, $q, $sessionStorage, logger, exception) {
        var request = function (method, url, data) {
            var config = {
                dataType: 'json',
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            };

            if (method === 'GET') {
                config.params = data;

            } else {
                config.data = data;
            }

            if ($sessionStorage.auth_key) {
                config.url = url + '?auth_key=' + $sessionStorage.auth_key;
            } else {
                config.url = url;
            }

            return $http(config).then(
                function (response) {
                    var defer = $q.defer();
                    logger.log('response', url, response);
                    if (response.data.error) {
                        exception.catcher(response.data.error)();
                        defer.reject(response.data.error);
                    } else {
                        defer.resolve(response.data);
                    }
                    return defer.promise;
                },
                function (response) {
                    var defer = $q.defer();
                    if (response.status === 200) {
                        exception.catcher('Server Error: ' + response.data)();
                        defer.reject(response.data);
                    } else if (response.status === -1) {
                        exception.catcher('Server unavailable')();
                        defer.reject(response.data);
                    } else if (response.status === 500) {
                        exception.catcher('Server Error: ' + response.status + ' ' + response.data.message)();
                        defer.reject(response.data);
                    } else if (response.status === 403) {
                        exception.catcher('Access denied.')();
                        defer.reject(response.data);
                    } else {
                        exception.catcher('Server Error: ' + response.status + ' ' + response.statusText)();
                        defer.reject(response.data);
                    }
                    defer.reject(response.data);
                    return defer.promise;
                }
            );
        };
        var requestFile = function (url, data) {
            var config = {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            };
            if ($sessionStorage.auth_key) {
                url = url + '?auth_key=' + $sessionStorage.auth_key;
            }

            return $http.post(url, data, config).then(
                function (response) {
                    var defer = $q.defer();
                    logger.log('response', url, response);

                    if (response.data.error) {
                        exception.catcher(response.data.error)();
                        defer.reject(response.data.error);
                    }
                    defer.resolve(response.data);
                    return defer.promise;
                },
                function (response) {
                    var defer = $q.defer();

                    if (response.status === 200) {
                        exception.catcher('Server Error: ' + response.data)();
                        defer.reject(response.data);
                    } else if (response.status === -1) {
                        exception.catcher('Server unavailable')();
                        defer.reject(response.data);
                    } else if (response.status === 500) {
                        exception.catcher('Server Error: ' + response.status + ' ' + response.data.message)();
                        defer.reject(response.data);
                    } else if (response.status === 403) {
                        exception.catcher('Access denied.')();
                        defer.reject(response.data);
                    } else {
                        exception.catcher('Server Error: ' + response.status + ' ' + response.statusText)();
                        defer.reject(response.data);
                    }
                    defer.reject(response.data);
                    return defer.promise;
                }
            );
        };

        return {
            get: function (url, data) {
                return request('GET', url, data);
            },
            post: function (url, data) {
                return request('POST', url, data);
            },
            delete: function (url, data) {
                return request('DELETE', url, data);
            },
            put: function (url, data) {
                return request('PUT', url, data);
            },
            file: function (url, data) {
                return requestFile(url, data);
            }
        };
    }

})();
