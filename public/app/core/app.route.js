(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());

        function getRoutes() {
            return [
                {
                    name: 'dashboard',
                    config: {
                        url: '/dashboard',
                        templateUrl: 'app/templates/dashboard/dashboard.html'
                    }
                }
            ];
        }
    }

})();
